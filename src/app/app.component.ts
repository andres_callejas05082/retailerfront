import { Component } from '@angular/core';
import {  FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { PostsService } from './services/posts.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export  class  AppComponent {

  arrPostsGif: any[];
  formulario: FormGroup;

  constructor(private postsService: PostsService) { 
    this.formulario = new FormGroup({
      retailer: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      zipcode: new FormControl(''),
      phone: new FormControl('')
    });
  }


  ngOnInit() {
      this.postsService.getAll()
        .then(posts => this.arrPostsGif = posts)
        .catch(error => console.log(error));

  }

  async onSubmit(){
    try {
      const respuesta = await this.postsService.create(this.formulario.value);
      console.log(respuesta)
    } catch (error) {
      console.log(error)
    }
    

  }



}
