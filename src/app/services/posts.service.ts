import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  baseUrlUser: string;
  baseUrlGif: string;
  baseUrlList: string;

  constructor(private httpClient: HttpClient) { 
    this.baseUrlGif ='http://127.0.0.1:8000/user/gc/';
    this.baseUrlUser = 'http://127.0.0.1:8000/user/';
    this.baseUrlList = 'http://127.0.0.1:8000/user/giftcardinfo-viewset/';
  }

  getAll(): Promise<any >{
    return this.httpClient.get<any >(this.baseUrlList).toPromise();
  }

  create({retailer,email,password,zipcode,phone}): Promise<any>{
    const userRequest = {retailer,email,password,zipcode,phone};
    return this.httpClient.post<any>(this.baseUrlUser,userRequest).toPromise();
  }
}
